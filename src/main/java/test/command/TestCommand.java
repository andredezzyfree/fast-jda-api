package test.command;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.features.command.protocol.Socket;
import com.andredezzy.fast.features.command.type.model.FastType;
import test.command.type.TestType;

public class TestCommand extends FastCommand {

    @Override
    public FastType getType() {
        return new TestType();
    }

    @Override
    public void execute(Socket socket, String[] args) {
        Fast.getMessage("%s || %s", getType().getName(), getType().getDescription()).debug();
        Fast.getMessage(getDescription()).debug();
    }
}
