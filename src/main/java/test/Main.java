package test;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.init.FastApplication;
import com.andredezzy.fast.init.JavaBot;
import test.command.TestCommand;
import test.command.type.TestType;

public class Main extends JavaBot {

    public void onStartup() {
        Fast.getMessage("starting...").debug();
        getType("Test").setSuper(new TestType());
        getCommand("test").setExecutor(new TestCommand());
    }

    public void onRun() {
        Fast.getMessage("running...").debug();
    }

    public void onStop() {
        Fast.getMessage("stopping...").debug();
    }

    public static void main(String[] args) {
        FastApplication.run(Main.class, args);
    }
}
