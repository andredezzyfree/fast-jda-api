package com.andredezzy.fast.features.reaction.manager;

import com.andredezzy.fast.features.reaction.model.ReactionMessage;

import java.util.ArrayList;
import java.util.List;

public class ReactionManager {

    private List<ReactionMessage> reactionMessages;

    public ReactionManager() {
        reactionMessages = new ArrayList<>();
    }

    public List<ReactionMessage> getReactionMessages() {
        return reactionMessages;
    }
}
