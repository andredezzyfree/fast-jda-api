package com.andredezzy.fast;

import com.andredezzy.fast.util.Messenger;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Fast {

    private Fast() {
    }

    public static Messenger getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return new Messenger.Builder(sw.getBuffer().toString()).build();
    }

    public static Messenger getMessage(Object object, Object... objects){
        return new Messenger.Builder(object, objects).build();
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            getStackTrace(e).error();
            Thread.currentThread().interrupt();
        }
    }
}
