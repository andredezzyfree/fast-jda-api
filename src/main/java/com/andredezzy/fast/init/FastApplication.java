package com.andredezzy.fast.init;

import com.andredezzy.fast.features.Feature;
import com.andredezzy.fast.features.command.CommandFeature;
import com.andredezzy.fast.features.command.defaults.EmoteCommand;
import com.andredezzy.fast.features.command.defaults.HelpCommand;
import com.andredezzy.fast.features.command.type.defaults.OtherType;
import com.andredezzy.fast.features.reaction.ReactionFeature;
import com.andredezzy.fast.features.reaction.manager.ReactionManager;
import com.andredezzy.fast.init.yaml.YamlLoader;
import com.andredezzy.fast.init.yaml.defaults.BotYamlDefault;
import lombok.Data;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import org.apache.log4j.Logger;

@Data
public class FastApplication {

    private static Logger logger;
    private static JavaBot init;
    private static Feature.Manager featureManager;
    private static ReactionManager reactionManager;

    public static void run(Class<? extends JavaBot> clazz, String[] args) {
        try {
            FastApplication initializer = new FastApplication();

            setLogger(Logger.getLogger(FastApplication.class));
            BotYamlDefault botYaml = (BotYamlDefault) new YamlLoader().load("bot.yml", BotYamlDefault.class);

            JavaBot javaBot = new JavaBot.Loader().load(botYaml.getMain());
            javaBot.setConfiguration(botYaml);
            javaBot.setLogger(getLogger());

            setInitialized(javaBot);
            javaBot.onStartup();

            JDA jdaBuilded = new JavaBot.Builder().build(javaBot);
            jdaBuilded.setAutoReconnect(true);
            Guild defaultGuild = null;
            if (botYaml.getDefaultGuildId() != null)
                defaultGuild = jdaBuilded.getGuildById(botYaml.getDefaultGuildId());
            String inviteUrl = jdaBuilded.asBot().getApplicationInfo().complete(true).getInviteUrl(Permission.ADMINISTRATOR);

            javaBot.setDefaultGuild(defaultGuild);
            javaBot.setInviteUrl(inviteUrl);

            setFeatureManager(new Feature.Manager());
            setReactionManager(new ReactionManager());

            initializer.defaultsRegistration();
            initializer.featuresRegistration();

            javaBot.onRun();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void defaultsRegistration() {
        getInitialized().getType("Outros").setSuper(new OtherType());
        getInitialized().getCommand("help").setExecutor(new HelpCommand());
        getInitialized().getCommand("emote").setExecutor(new EmoteCommand());
    }

    private void featuresRegistration() {
        getFeatureManager().addFeature(new CommandFeature());
        getFeatureManager().addFeature(new ReactionFeature());
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        FastApplication.logger = logger;
    }

    public static JavaBot getInitialized() {
        return init;
    }

    public static void setInitialized(JavaBot init) {
        FastApplication.init = init;
    }

    public static Feature.Manager getFeatureManager() {
        return featureManager;
    }

    public static void setFeatureManager(Feature.Manager featureManager) {
        FastApplication.featureManager = featureManager;
    }

    public static ReactionManager getReactionManager() {
        return reactionManager;
    }

    public static void setReactionManager(ReactionManager reactionManager) {
        FastApplication.reactionManager = reactionManager;
    }
}