package com.andredezzy.fast.util;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

public class Embed extends EmbedBuilder {

    public EmbedBuilder setAuthor(User user) {
        super.setAuthor(user.getName(), null, user.getAvatarUrl());
        return this;
    }
}
