package com.andredezzy.fast.init.yaml;

import java.io.IOException;

public class FileNotFoundException extends IOException {

    public FileNotFoundException(String file) {
        super(String.format("Configuration file '%s' not founded.", file));
    }
}
