package com.andredezzy.fast.init.exception;

public class InvalidBotTokenException extends Exception {

    public InvalidBotTokenException(String name, String token) {
        super(String.format("%s's token (%s) is invalid.", name, token));
    }
}
