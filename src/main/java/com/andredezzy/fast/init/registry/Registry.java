package com.andredezzy.fast.init.registry;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.features.command.exception.CommandNotFoundException;
import com.andredezzy.fast.features.command.protocol.CommandRegistry;
import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.features.command.type.protocol.TypeRegistry;
import com.andredezzy.fast.init.JavaBot;
import com.andredezzy.fast.init.yaml.defaults.BotYamlDefault;
import com.andredezzy.fast.init.yaml.defaults.CommandYamlDefault;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Registry {

    private JavaBot init;
    private List<FastCommand> commands;
    private List<FastType> types;

    protected Registry() {
        this.commands = new ArrayList<>();
        this.types = new ArrayList<>();
    }

    public CommandRegistry getCommand(String command) {
        try {
            BotYamlDefault initConfiguration = this.init.getConfiguration();
            CommandYamlDefault commandYaml = initConfiguration.getCommands().get(command);

            if (commandYaml == null)
                throw new CommandNotFoundException(command);

            commandYaml.setCommand(command);

            return new CommandRegistry(this, commandYaml);
        } catch (Exception e) {
            Fast.getStackTrace(e).error();
        }
        return null;
    }

    public TypeRegistry getType(String type) {
        try {
            if (type == null || type.length() <= 0)
                throw new UnexpectedException("Occurred a unexpected error on 'TypeRegistry', method 'setSuper' (null type).");

            return new TypeRegistry(this, type);
        } catch (UnexpectedException e) {
            Fast.getStackTrace(e).error();
        }
        return null;
    }

    public List<FastCommand> getCommandsFromType(FastType type) {
        return getCommands().stream().filter(command -> command.getType().getName().equalsIgnoreCase(type.getName()) &&
                command.getType().getDescription().equalsIgnoreCase(type.getDescription())).collect(Collectors.toList());
    }

    public List<FastCommand> getCommands() {
        return this.commands;
    }

    public List<FastType> getTypes() {
        return this.types;
    }

    protected void setRegister(JavaBot init) {
        this.init = init;
    }
}
