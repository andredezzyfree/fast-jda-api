package com.andredezzy.fast.features.reaction.model;

import com.andredezzy.fast.features.reaction.protocol.listener.ReactionAddListener;
import com.andredezzy.fast.features.reaction.protocol.listener.ReactionRemoveListener;

import java.util.function.Consumer;

public class ReactionMessage {

    private String messageId;
    private Consumer<ReactionAddListener> reactionAdd;
    private Consumer<ReactionRemoveListener> reactionRemove;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Consumer<ReactionAddListener> getReactionAdd() {
        return reactionAdd;
    }

    public void setReactionAdd(Consumer<ReactionAddListener> reactionAdd) {
        this.reactionAdd = reactionAdd;
    }

    public Consumer<ReactionRemoveListener> getReactionRemove() {
        return reactionRemove;
    }

    public void setReactionRemove(Consumer<ReactionRemoveListener> reactionRemove) {
        this.reactionRemove = reactionRemove;
    }

    @Override
    public String toString() {
        return String.format("ReactionMessage{messageId='%s'}", messageId);
    }
}
