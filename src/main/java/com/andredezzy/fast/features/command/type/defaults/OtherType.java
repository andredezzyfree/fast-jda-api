package com.andredezzy.fast.features.command.type.defaults;

import com.andredezzy.fast.features.command.type.model.FastType;

public class OtherType extends FastType {
    @Override
    public String getName() {
        return "Outros";
    }

    @Override
    public String getDescription() {
        return "Outros comandos, não relacionados à nenhum tipo anterior.";
    }
}
