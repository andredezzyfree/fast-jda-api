package com.andredezzy.fast.init.yaml.defaults;

import java.util.HashMap;
import java.util.Map;

public class BotYamlDefault {

    private String name;
    private String token;
    private String main;
    private String description;
    private String defaultGuildId;
    private String commandPrefix;
    private Map<String, CommandYamlDefault> commands;

    public BotYamlDefault() {
        this.name = "null";
        this.description = "null";
        this.commands = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultGuildId() {
        return defaultGuildId;
    }

    public void setDefaultGuildId(String defaultGuildId) {
        this.defaultGuildId = defaultGuildId;
    }

    public String getCommandPrefix() {
        return commandPrefix;
    }

    public void setCommandPrefix(String commandPrefix) {
        this.commandPrefix = commandPrefix;
    }

    public Map<String, CommandYamlDefault> getCommands() {
        return commands;
    }

    public void setCommands(Map<String, CommandYamlDefault> commands) {
        this.commands = commands;
    }
}
