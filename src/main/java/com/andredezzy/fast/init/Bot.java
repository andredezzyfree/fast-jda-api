package com.andredezzy.fast.init;

import com.andredezzy.fast.init.registry.Registry;
import com.andredezzy.fast.init.yaml.defaults.BotYamlDefault;
import lombok.Data;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import org.apache.log4j.Logger;

@Data
public abstract class Bot extends Registry {

    private JDA jda;
    private Guild defaultGuild;
    private String inviteUrl;
    private BotYamlDefault configuration;
    private Logger logger;

    public Bot() {
        super();
    }

    public abstract void onStartup();

    public abstract void onRun();

    public abstract void onStop();

    public JDA getJda() {
        return jda;
    }

    public void setJda(JDA jda) {
        this.jda = jda;
    }

    public Guild getDefaultGuild() {
        return defaultGuild;
    }

    public void setDefaultGuild(Guild defaultGuild) {
        this.defaultGuild = defaultGuild;
    }

    public String getInviteUrl() {
        return inviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        this.inviteUrl = inviteUrl;
    }

    public BotYamlDefault getConfiguration() {
        return configuration;
    }

    public void setConfiguration(BotYamlDefault configuration) {
        this.configuration = configuration;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
