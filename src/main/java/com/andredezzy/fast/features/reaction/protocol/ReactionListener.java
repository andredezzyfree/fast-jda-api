package com.andredezzy.fast.features.reaction.protocol;

public interface ReactionListener {

    String getReaction();
}
