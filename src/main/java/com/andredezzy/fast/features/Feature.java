package com.andredezzy.fast.features;

import com.andredezzy.fast.init.FastApplication;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface Feature {

    String getName();

    String getDescription();

    ListenerAdapter getListener();

    public static class Manager {

        private List<Feature> registeredFeatures;

        public Manager() {
            registeredFeatures = new ArrayList<>();
        }

        public Feature addFeature(Feature feature) {
            registeredFeatures.add(feature);
            if (feature.getListener() != null)
                FastApplication.getInitialized().getJda().addEventListener(feature.getListener());
            return feature;
        }

        public List<Feature> addFeatures(Feature... features) {
            registeredFeatures.addAll(Arrays.asList(features));
            FastApplication.getInitialized().getJda().addEventListener(Arrays.stream(features).filter(feature -> feature.getListener() != null)
                    .map(Feature::getListener).collect(Collectors.toList()));
            return Arrays.asList(features);
        }
    }
}
