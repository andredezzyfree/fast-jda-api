package com.andredezzy.fast.features.reaction.protocol.listener;

import com.andredezzy.fast.features.reaction.protocol.ReactionListener;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;

public class ReactionAddListener implements ReactionListener {

    private MessageReactionAddEvent addEvent;

    @Override
    public String getReaction() {
        return getEvent().getReactionEmote().getName();
    }

    public MessageReactionAddEvent getEvent() {
        return addEvent;
    }

    public void setEvent(MessageReactionAddEvent addEvent) {
        this.addEvent = addEvent;
    }
}
