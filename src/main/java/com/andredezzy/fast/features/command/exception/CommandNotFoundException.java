package com.andredezzy.fast.features.command.exception;

public class CommandNotFoundException extends Exception {

    public CommandNotFoundException(String command) {
        super(String.format("Command '%s' not found.", command));
    }
}
