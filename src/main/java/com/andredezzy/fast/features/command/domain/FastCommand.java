package com.andredezzy.fast.features.command.domain;

import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.features.command.protocol.Socket;
import com.andredezzy.fast.init.yaml.defaults.CommandYamlDefault;

public abstract class FastCommand extends CommandYamlDefault {

    public abstract FastType getType();

    public abstract void execute(Socket socket, String[] args);

    @Override
    public String toString() {
        return String.format("FastCommand{command='%s', description='%s', type='%s'}", getCommand(), getDescription(), getType());
    }
}