package com.andredezzy.fast.util;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatColor {

    private static final String RESET = "\u001B[0m";

    public static String parseColors(String input) {
        String colorsString = input;
        Pattern regexChecker = Pattern.compile(":\\S+:");
        Matcher regexMatcher = regexChecker.matcher(input);

        while (regexMatcher.find())
            if (regexMatcher.group().length() > 0) {
                String color = regexMatcher.group().trim().replace(":", "");

                colorsString = colorsString.replace(regexMatcher.group().trim(), "\u001B[3" + Color.fromName(color) + ";1m").replace("[RC]", RESET);
            }
        return String.format("%s%s", colorsString, RESET);
    }

    private enum Color {

        BLACK(0), RED(1), GREEN(2), YELLOW(3), BLUE(4), MAGENTA(5), CYAN(6), WHITE(7);

        private int code;

        Color(int code) {
            this.code = code;
        }

        public static int fromName(String color) {
            return Arrays.stream(values()).filter(c -> c.name().equalsIgnoreCase(color)).map(Color::getCode).findFirst().orElse(7);
        }

        public int getCode() {
            return code;
        }
    }
}