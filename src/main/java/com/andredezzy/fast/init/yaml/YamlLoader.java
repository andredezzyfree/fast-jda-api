package com.andredezzy.fast.init.yaml;

import org.yaml.snakeyaml.error.YAMLException;

import java.io.IOException;
import java.io.InputStream;

public class YamlLoader {

    public Object load(String file, Class<?> clazz) throws FileNotFoundException {
        org.yaml.snakeyaml.Yaml yaml = new org.yaml.snakeyaml.Yaml();
        ClassLoader classLoader = getClass().getClassLoader();
        try (InputStream in = classLoader.getResourceAsStream(file)) {
            return yaml.loadAs(in, clazz);
        } catch (IOException | YAMLException e) {
            throw new FileNotFoundException(file);
        }
    }
}
