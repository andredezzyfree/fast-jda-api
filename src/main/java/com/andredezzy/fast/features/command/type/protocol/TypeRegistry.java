package com.andredezzy.fast.features.command.type.protocol;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.command.type.exception.TypeAlreadyRegisteredException;
import com.andredezzy.fast.features.command.type.exception.TypeNotCorrespondentException;
import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.init.registry.Registry;

import java.util.ArrayList;
import java.util.List;

public class TypeRegistry {

    private Registry registry;
    private String type;

    public TypeRegistry(Registry registry, String type) {
        this.registry = registry;
        this.type = type;
    }

    public void setSuper(FastType commandType) {
        try {
            if (!commandType.getName().equalsIgnoreCase(type))
                throw new TypeNotCorrespondentException(commandType.getName(), this.type);

            if (!this.registry.getTypes().contains(commandType)) {
                this.registry.getTypes().add(commandType);
            } else throw new TypeAlreadyRegisteredException(commandType.getName());
        } catch (Exception e) {
            Fast.getStackTrace(e).error();
        }
    }
}
