package com.andredezzy.fast.init.exception;

public class MainNotFoundException extends Exception {

    public MainNotFoundException(String main) {
        super(String.format("Bot main class '%s' not founded.", main));
    }
}
