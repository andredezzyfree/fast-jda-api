package com.andredezzy.fast.features.command.exception;

public class CommandAlreadyRegisteredException extends Exception {

    public CommandAlreadyRegisteredException(String type) {
        super(String.format("Command '%s' already registered.", type));
    }
}
