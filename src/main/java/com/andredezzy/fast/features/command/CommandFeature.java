package com.andredezzy.fast.features.command;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.Feature;
import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.features.command.protocol.Socket;
import com.andredezzy.fast.init.FastApplication;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CommandFeature implements Feature {


    @Override
    public String getName() {
        return "Command";
    }

    @Override
    public String getDescription() {
        return "A simple way to make discord commands.";
    }

    @Override
    public ListenerAdapter getListener() {
        return new ReceivedMessage();
    }

    private static class ReceivedMessage extends ListenerAdapter {

        @Override
        public void onMessageReceived(MessageReceivedEvent event) {
            try {
                String[] args = event.getMessage().getContentRaw().split(" ");
                String commandPrefix = FastApplication.getInitialized().getConfiguration().getCommandPrefix();

                if (args[0].startsWith(commandPrefix)) {
                    event.getMessage().delete().queue();
                    String executedCommand = args[0].replaceAll(commandPrefix, "");

                    List<FastCommand> registriedCommands = FastApplication.getInitialized().getCommands();
                    FastCommand executed = registriedCommands.stream().filter(fastCommand -> fastCommand.getCommand()
                            .equalsIgnoreCase(executedCommand)).findFirst().orElse(null);

                    if (executed != null) {
                        Socket socket = new Socket();
                        socket.setUser(event.getAuthor());
                        socket.setGuild(event.getGuild());
                        socket.setChannel(event.getTextChannel());
                        socket.setMessage(event.getMessage());

                        List<String> argsList = new LinkedList<>(Arrays.asList(args));
                        argsList.remove(0);

                        executed.execute(socket, argsList.toArray(new String[0]));
                    } else {
                        Fast.getMessage("Command not found: %s", executedCommand).error();
                    }
                }
            } catch (Exception e) {
                Fast.getStackTrace(e).error();
            }
        }
    }
}
