package com.andredezzy.fast.init.yaml.defaults;

public class CommandYamlDefault {

    private String command;
    private String description;

    public CommandYamlDefault() {
        this.description = "No description found.";
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("{description[%s]}", description);
    }
}
