package com.andredezzy.fast.features.command.protocol;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.init.FastApplication;
import com.andredezzy.fast.features.command.exception.CommandAlreadyRegisteredException;
import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.features.command.type.exception.TypeNotFoundException;
import com.andredezzy.fast.init.registry.Registry;
import com.andredezzy.fast.init.yaml.defaults.CommandYamlDefault;

import java.util.ArrayList;
import java.util.List;

public class CommandRegistry {

    private Registry registry;
    private CommandYamlDefault commandYaml;

    public CommandRegistry(Registry registry, CommandYamlDefault commandYaml) {
        this.registry = registry;
        this.commandYaml = commandYaml;
    }

    public void setExecutor(FastCommand fastCommand) {
        try {
            fastCommand.setCommand(this.commandYaml.getCommand());
            fastCommand.setDescription(this.commandYaml.getDescription());

            if (FastApplication.getInitialized().getTypes().stream().map(FastType::getName).noneMatch(s -> s.equalsIgnoreCase
                    (fastCommand.getType().getName())))
                throw new TypeNotFoundException(fastCommand.getType().getName());

            if (!this.registry.getCommands().contains(fastCommand)) {
                this.registry.getCommands().add(fastCommand);
            }
            else throw new CommandAlreadyRegisteredException(fastCommand.getCommand());
        } catch (Exception e) {
            Fast.getStackTrace(e).error();
        }
    }
}
