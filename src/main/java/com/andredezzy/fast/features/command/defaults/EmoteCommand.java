package com.andredezzy.fast.features.command.defaults;

import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.Fast;
import com.andredezzy.fast.features.command.protocol.Socket;
import com.andredezzy.fast.features.command.type.defaults.OtherType;
import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.features.reaction.builder.ReactionBuilder;
import com.andredezzy.fast.util.Embed;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

import java.awt.*;

public class EmoteCommand extends FastCommand {
    @Override
    public FastType getType() {
        return new OtherType();
    }

    @Override
    public void execute(Socket socket, String[] args) {
        try {
            Embed reactHereResponse = new Embed();
            reactHereResponse.setAuthor(socket.getUser());
            reactHereResponse.setColor(Color.MAGENTA);
            reactHereResponse.setTitle("Add a reaction in that message to copy her.");
            reactHereResponse.setDescription("Adicione uma reação nesta mensagem para copiá-la.");

            Message messageEmbed = socket.getChannel().sendMessage(reactHereResponse.build()).complete(true);

            ReactionBuilder reactionBuilder = new ReactionBuilder(messageEmbed.getId());
            reactionBuilder.onAddReaction(reaction -> {
                Fast.getMessage("Added reaction: %s", reaction.getReaction()).debug();
                socket.getChannel().sendMessage(String.format("Added reaction: ```%s```", reaction
                        .getReaction())).queue();
            });
        } catch (RateLimitedException e) {
            Fast.getStackTrace(e).error();
        }
    }
}
