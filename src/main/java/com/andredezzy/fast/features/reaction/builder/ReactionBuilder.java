package com.andredezzy.fast.features.reaction.builder;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.init.FastApplication;
import com.andredezzy.fast.features.reaction.manager.ReactionManager;
import com.andredezzy.fast.features.reaction.model.ReactionMessage;
import com.andredezzy.fast.features.reaction.protocol.listener.ReactionAddListener;
import com.andredezzy.fast.features.reaction.protocol.listener.ReactionRemoveListener;

import java.util.function.Consumer;

public class ReactionBuilder {

    private ReactionManager reactionManager;
    private ReactionMessage reactionMessage;

    public ReactionBuilder(String messageId) {
        this.reactionManager = FastApplication.getReactionManager();
        this.reactionMessage = new ReactionMessage();
        this.reactionMessage.setMessageId(messageId);
    }

    public void onAddReaction(Consumer<ReactionAddListener> onAdd) {
        new Thread(() -> {
            Fast.sleep(500);
            this.reactionMessage.setReactionAdd(onAdd);
            this.reactionManager.getReactionMessages().add(this.reactionMessage);
        }).start();
    }

    public void onRemoveReaction(Consumer<ReactionRemoveListener> onRemove) {
        new Thread(() -> {
            Fast.sleep(500);
            this.reactionMessage.setReactionRemove(onRemove);
            this.reactionManager.getReactionMessages().add(this.reactionMessage);
        }).start();
    }
}
