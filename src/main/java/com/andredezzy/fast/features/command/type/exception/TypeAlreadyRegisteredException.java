package com.andredezzy.fast.features.command.type.exception;

public class TypeAlreadyRegisteredException extends Exception {

    public TypeAlreadyRegisteredException(String type) {
        super(String.format("Type '%s' already registered.", type));
    }
}
