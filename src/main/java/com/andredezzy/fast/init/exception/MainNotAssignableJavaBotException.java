package com.andredezzy.fast.init.exception;

public class MainNotAssignableJavaBotException extends Exception {

    public MainNotAssignableJavaBotException(String main) {
        super(String.format("Bot main class '%s' not assignable from JavaBot.", main));
    }
}
