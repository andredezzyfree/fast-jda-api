package com.andredezzy.fast.features.command.type.exception;

public class TypeNotCorrespondentException extends Exception {

    public TypeNotCorrespondentException(String type, String informedType) {
        super(String.format("Type '%s' not correspond to the informed (%s).", type, informedType));
    }
}
