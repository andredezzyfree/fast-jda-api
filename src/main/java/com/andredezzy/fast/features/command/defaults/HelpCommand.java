package com.andredezzy.fast.features.command.defaults;

import com.andredezzy.fast.features.command.domain.FastCommand;
import com.andredezzy.fast.Fast;
import com.andredezzy.fast.init.FastApplication;
import com.andredezzy.fast.features.command.protocol.Socket;
import com.andredezzy.fast.features.command.type.defaults.OtherType;
import com.andredezzy.fast.features.command.type.model.FastType;
import com.andredezzy.fast.features.reaction.builder.ReactionBuilder;
import com.andredezzy.fast.util.Embed;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class HelpCommand extends FastCommand {

    @Override
    public FastType getType() {
        return new OtherType();
    }

    @Override
    public void execute(Socket socket, String[] args) {
        try {
            Embed helpResponse = new Embed();
            helpResponse.setAuthor(socket.getUser());
            helpResponse.setColor(Color.YELLOW);
            helpResponse.setTitle("Tipos de comandos:");

            List<String> emoteNumberList = Arrays.asList("1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "\uD83D\uDD1F");

            Map<String, FastType> typeIds = new HashMap<>();

            StringBuilder typeStringBuilder = new StringBuilder();
            AtomicInteger typeCount = new AtomicInteger(1);
            FastApplication.getInitialized().getTypes().forEach(type -> {
                int id = typeCount.getAndIncrement();
                typeStringBuilder.append(String.format("%s. %s - %s%s", id, type.getName(), type.getDescription(), "\n"));
                typeIds.put(emoteNumberList.get(id - 1), type);
            });

            helpResponse.setDescription(typeStringBuilder.toString());
            int deleteTime = typeCount.get() <= 2 ? typeCount.get() * 3 : typeCount.get() * 2;

            Message messageEmbed = null;
            messageEmbed = socket.getChannel().sendMessage(helpResponse.build()).complete(true);
            messageEmbed.delete().queueAfter(deleteTime, TimeUnit.SECONDS);

            for (int i = 0; i < (typeCount.get() - 1); i++) messageEmbed.addReaction(emoteNumberList.get(i)).complete(true);

            ReactionBuilder reactionBuilder = new ReactionBuilder(messageEmbed.getId());
            reactionBuilder.onAddReaction(onAdd -> {
                FastType type = typeIds.get(onAdd.getReaction());
                List<FastCommand> commands = FastApplication.getInitialized().getCommandsFromType(type);

                Embed commandsResponse = new Embed();
                commandsResponse.setAuthor(socket.getUser());
                commandsResponse.setColor(Color.GREEN);
                commandsResponse.setTitle(String.format("Comandos do tipo '%s':", type.getName()));

                StringBuilder commandStringBuilder = new StringBuilder();
                AtomicInteger commandCount = new AtomicInteger(1);
                commands.forEach(command -> {
                    commandCount.getAndIncrement();
                    commandStringBuilder.append(String.format("- %s - %s%s", command.getCommand(), command.getDescription(), "\n"));
                });

                commandsResponse.setDescription(commandStringBuilder.toString());

                int deleteCommandTime = commandCount.get() <= 2 ? commandCount.get() * 3 : commandCount.get() * 2;
                try {
                    socket.getChannel().sendMessage(commandsResponse.build()).complete(true).delete().queueAfter(deleteCommandTime, TimeUnit.SECONDS);
                } catch (RateLimitedException e) {
                    Fast.getStackTrace(e).error();
                }
            });
        } catch (RateLimitedException e) {
            Fast.getStackTrace(e).error();
        }
    }
}
