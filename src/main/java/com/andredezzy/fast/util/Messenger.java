package com.andredezzy.fast.util;

import com.andredezzy.fast.init.FastApplication;

import java.util.Arrays;

public class Messenger {

    private Object object;
    private Object[] objects;

    private Messenger(Object object, Object... objects) {
        this.object = object;
        this.objects = objects;
    }

    public void info() {
        print(":yellow:[INFO]");
    }

    public void debug() {
        print(":blue:[DEBUG]");
    }

    public void error() {
        print(":red:[ERROR]");
    }

    public void print(String prefix) {
        FastApplication.getLogger().info(ChatColor.parseColors(String.format("%s -> [RC]%s", prefix,
                String.format(this.object.toString(), this.objects))));
    }

    public static class Builder{

        private Object object;
        private Object[] objects;

        public Builder(Object object, Object... objects){
            this.object = object;
            this.objects = objects;
        }

        public Messenger build(){
            return new Messenger(this.object, this.objects);
        }
    }
}
