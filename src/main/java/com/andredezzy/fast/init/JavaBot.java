package com.andredezzy.fast.init;

import com.andredezzy.fast.Fast;
import com.andredezzy.fast.init.exception.InvalidBotTokenException;
import com.andredezzy.fast.init.exception.MainNotAssignableJavaBotException;
import com.andredezzy.fast.init.exception.MainNotFoundException;
import com.andredezzy.fast.init.exception.NullBotTokenException;
import com.andredezzy.fast.init.registry.Registry;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;

public abstract class JavaBot extends Bot {

    public JavaBot() {
        setRegister(this);
    }

    public static class Builder {

        public JDA build(JavaBot javaBot) throws NullBotTokenException, InvalidBotTokenException {
            try {
                if (javaBot.getConfiguration().getToken() == null)
                    throw new NullBotTokenException(javaBot.getConfiguration().getName());
                JDA buildedJda = new JDABuilder(AccountType.BOT).setToken(javaBot.getConfiguration().getToken()).buildBlocking();
                javaBot.setJda(buildedJda);
                return buildedJda;
            } catch (LoginException login) {
                throw new InvalidBotTokenException(javaBot.getConfiguration().getName(), javaBot.getConfiguration().getToken());
            } catch (InterruptedException e) {
                Fast.getStackTrace(e).error();
                Fast.getMessage("Error on building JDA.").error();
                Thread.currentThread().interrupt();
            }
            return null;
        }
    }

    public static class Loader {

        public JavaBot load(String mainClass) throws MainNotFoundException {
            try {
                Class<?> javaBotWrapper = Class.forName(mainClass);
                if (JavaBot.class.isAssignableFrom(javaBotWrapper)) {
                    return (JavaBot) javaBotWrapper.newInstance();
                } else throw new MainNotAssignableJavaBotException(mainClass);
            } catch (Exception e) {
                throw new MainNotFoundException(mainClass);
            }
        }
    }
}
