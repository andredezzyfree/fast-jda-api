package com.andredezzy.fast.init.exception;

public class NullBotTokenException extends Exception {

    public NullBotTokenException(String name) {
        super(String.format("%s's token not found.", name));
    }
}
