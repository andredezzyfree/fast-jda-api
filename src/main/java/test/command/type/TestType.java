package test.command.type;

import com.andredezzy.fast.features.command.type.model.FastType;

public class TestType extends FastType {

    @Override
    public String getName() {
        return "Test";
    }

    @Override
    public String getDescription() {
        return "Test commands.";
    }
}
