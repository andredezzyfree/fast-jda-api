package com.andredezzy.fast.features.command.type.model;

public abstract class FastType {

    public abstract String getName();

    public abstract String getDescription();

    @Override
    public String toString() {
        return String.format("FastType{name='%s', description='%s'}", getName(), getDescription());
    }
}
