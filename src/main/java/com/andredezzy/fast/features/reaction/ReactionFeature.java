package com.andredezzy.fast.features.reaction;

import com.andredezzy.fast.features.reaction.manager.ReactionManager;
import com.andredezzy.fast.init.FastApplication;
import com.andredezzy.fast.features.Feature;
import com.andredezzy.fast.features.reaction.model.ReactionMessage;
import com.andredezzy.fast.features.reaction.protocol.listener.ReactionAddListener;
import com.andredezzy.fast.features.reaction.protocol.listener.ReactionRemoveListener;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.List;

public class ReactionFeature implements Feature {
    @Override
    public String getName() {
        return "Reaction";
    }

    @Override
    public String getDescription() {
        return "A simple way to detect on message reactions.";
    }

    @Override
    public ListenerAdapter getListener() {
        return new MessageReact();
    }

    private static class MessageReact extends ListenerAdapter {

        @Override
        public void onMessageReactionAdd(MessageReactionAddEvent event) {
            ReactionManager manager = FastApplication.getReactionManager();
            List<ReactionMessage> reactionMessages = manager.getReactionMessages();

            ReactionMessage reactionMessage = reactionMessages.stream().filter(rm -> rm.getMessageId().equalsIgnoreCase(event.getMessageId()))
                    .findFirst().orElse(null);

            if (reactionMessage != null && reactionMessage.getReactionAdd() != null) {
                ReactionAddListener addListener = new ReactionAddListener();
                addListener.setEvent(event);

                reactionMessage.getReactionAdd().accept(addListener);
            }
        }

        @Override
        public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
            ReactionManager manager = new ReactionManager();
            List<ReactionMessage> reactionMessages = manager.getReactionMessages();

            ReactionMessage reactionMessage = reactionMessages.stream().filter(rm -> rm.getMessageId().equalsIgnoreCase(event.getMessageId()))
                    .findFirst().orElse(null);

            if (reactionMessage != null && reactionMessage.getReactionRemove() != null) {
                ReactionRemoveListener removeListener = new ReactionRemoveListener();
                removeListener.setEvent(event);

                reactionMessage.getReactionRemove().accept(removeListener);
            }
        }
    }
}
