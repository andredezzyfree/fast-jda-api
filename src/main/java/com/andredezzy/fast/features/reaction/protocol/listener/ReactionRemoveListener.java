package com.andredezzy.fast.features.reaction.protocol.listener;

import com.andredezzy.fast.features.reaction.protocol.ReactionListener;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;

public class ReactionRemoveListener implements ReactionListener {

    private MessageReactionRemoveEvent removeEvent;

    @Override
    public String getReaction() {
        return getEvent().getReactionEmote().getName();
    }

    public MessageReactionRemoveEvent getEvent() {
        return removeEvent;
    }

    public void setEvent(MessageReactionRemoveEvent removeEvent) {
        this.removeEvent = removeEvent;
    }
}
