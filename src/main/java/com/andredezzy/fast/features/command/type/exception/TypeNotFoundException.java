package com.andredezzy.fast.features.command.type.exception;

public class TypeNotFoundException extends Exception {

    public TypeNotFoundException(String type) {
        super(String.format("Type '%s' not found.", type));
    }
}
